[Panoramix](https://www.ircam.fr/recherche/equipes-recherche/eac/panoramix/) is a post-production workstation for 3-D audio contents.

<!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/6L7jMIU4Yt8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->

This tool offers a comprehensive environment for mixing, reverberating, and spatializing sound materials from different microphone systems: surround microphone trees, spot microphones, ambient miking, Higher Order Ambisonics capture.

Several 3-D spatialization techniques (VBAP, HOA, binaural) can be combined and mixed simultaneously in different formats. Panoramix also provides conventional features of mixing engines (equalizer, compressor/expander, grouping parameters, routing of input/output signals, etc.), and it can be controlled entirely via the Open Sound Control protocol.

The software can also be used to control the diffusion of sound for spatialized live events.

## Main features ##

- up to 128 input/ 64 output channels
- unlimited number of tracks (mono, multichannel, Ambisonic microphones, etc.)
- unlimited number of spatialization buses (VBAP 2-D and 3-D, Higher-Order Ambisonic 2-D and 3-D, binaural, stereo, etc…)
- artificial reverberation based on Ircam Spat engine
-	grouping of parameters
-	parametric equalizer, compressor, expander available on each track and bus
-	flexible routing of input channels and buses
-	up to three bus send for each track
-	highly optimized rendering (low CPU)
-	OSC remote control
-	supports HRTF file under the AES-69 SOFA format


> **Design and Development:** [Acoustic and Cognitive Spaces]( https://www.ircam.fr/recherche/equipes-recherche/eac/) Team 
>
> **References**
>
>   - [Thibaut Carpentier](https://www.ircam.fr/person/thibaut-carpentier/). [Panoramix: 3D mixing and post-production workstation.](https://hal.archives-ouvertes.fr/hal-01366547v1) In Proc of 42nd International Computer Music Conference (ICMC), Utrecht, pp 122 – 127, Sept 2016.
>
>   - [Thibaut Carpentier](https://www.ircam.fr/person/thibaut-carpentier/), [Clément Cornuau](https://www.ircam.fr/person/clement-cornuau/). [Panoramix: station de mixage et post-production 3D.](https://hal.archives-ouvertes.fr/hal-01300314v1) In Proc of Journées d’Informatique Musicale (JIM), Albi, pp 162 – 169, March 2016.
>
>   - [Thibaut Carpentier](https://www.ircam.fr/person/thibaut-carpentier/). [A versatile workstation for the diffusion, mixing, and post-production of spatial audio.](https://hal.archives-ouvertes.fr/hal-01527754v1) In Proc of the Linux Audio Conference (LAC), Saint-Etienne, May 2017.
